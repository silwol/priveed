#![feature(plugin)]
#![plugin(rocket_codegen)]
#![feature(custom_derive)]

extern crate rocket;

use rocket::State;
use rocket::response::NamedFile;
use std::fmt;
use std::sync::mpsc;
use std::sync::{Arc,Mutex};
use std::thread;

enum ProcessingType {
    YoutubeDl,
    Embed,
    Mirror,
}

impl<'v> rocket::request::FromFormValue<'v> for ProcessingType {
    type Error = &'v str;
    fn from_form_value(form_value: &'v str) -> Result<Self, Self::Error>
    {
        match form_value.as_ref() {
            "youtube-dl" => Ok(ProcessingType::YoutubeDl),
            "embed" => Ok(ProcessingType::Embed),
            "mirror" => Ok(ProcessingType::Mirror),
            _ => Err(form_value)
        }
    }
}

#[derive(FromForm)]
struct UrlRequest {
    url: String,
    processing: ProcessingType
}

impl std::fmt::Display for UrlRequest {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result
    {
        write!(f, "{}", self.url)
    }
}

#[post("/submit", data="<url_request>")]
fn submit(
    tx: State<Arc<Mutex<mpsc::Sender<String>>>>,
    url_request: rocket::request::Form<UrlRequest>
    ) -> rocket::response::content::HTML<String>
{
    let processing: &str =
        match url_request.get().processing {
            ProcessingType::YoutubeDl => "Youtube Download",
            ProcessingType::Embed => "Embedded",
            ProcessingType::Mirror => "Mirror",
        };

    let request = url_request.get();

    let response = rocket::response::content::HTML(
        "<html>\
          <head>\
            <title>Priveed</title>\
          </head>\
          <body>\
          </body>\
        </html>".to_string() + url_request.get().url.as_str() + processing);

    let tx = tx.lock().unwrap();
    tx.send(url_request.get().url.to_string()).unwrap();

    response
}


#[get("/")]
fn index() -> std::io::Result<NamedFile> {
    NamedFile::open("static/index.html")
}

fn main() {
    println!("Hello, world!");

    let (tx, rx) = mpsc::channel::<String>();

    {
        thread::spawn(move || {
            loop {
                println!("received {}", rx.recv().unwrap());
            }
        });
    }

    let tx = Arc::new(Mutex::new(tx.clone()));

    rocket::ignite()
        .mount("/", routes![index, submit])
        .manage(tx)
        .launch();
}
